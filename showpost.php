<?php
include("connection.php"); 
include("ex2.php"); 

$postCRUD = new Post_CRUD($pdo); 

// Handle form submission
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = isset($_POST["title"]) ? $_POST["title"] : "";
    $content = isset($_POST["content"]) ? $_POST["content"] : "";
    
    if (!empty($title)) { 
        $authorId = 1; // Assuming author ID 1 for demo purposes
        $createdAt = date("Y-m-d H:i:s");

        $postCRUD->create($title, $content, $authorId, $createdAt);

        echo "Post added successfully!";
    } else {
        echo "Title cannot be empty!";
    }
}


if (isset($_GET["id"])) {
    $postId = $_GET["id"];
    $postCRUD->delete($postId);
    header("Location: showpost.php"); 
    exit();
}

$posts = $postCRUD->read(); 

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<form method="post" action="">
    <label for="title">Title:</label><br>
    <input type="text" id="title" name="title"><br>
    <label for="content">Content:</label><br>
    <textarea id="content" name="content"></textarea><br>
    <input type="submit" value="Submit">
</form>

<table>
    <tr>
        <th>Title</th>
        <th>Content</th>
        <th>Action</th>
    </tr>
    <?php foreach ($posts as $post): ?>
        <tr>
            <td><?php echo $post['title']; ?></td>
            <td><?php echo $post['content']; ?></td>
            <td><a href="showpost.php?id=<?php echo $post['id']; ?>">Delete</a></td>
        </tr>
    <?php endforeach; ?>
</table>

</body>
</html>
