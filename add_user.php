<?php
 include("connection.php");
 include("ex2.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<form action="" method="POST" role="form">
    <legend>Form title</legend>

    <div class="form-group">
        <label for="">name</label>
        <input type="text" class="form-control" name="username" id="" placeholder="Input field">
    </div>
    <div class="form-group">
        <label for="">email</label>
        <input type="text" class="form-control" name="email" id="email" placeholder="Input field">
    </div>
    <div class="form-group">
        <label for="">passwords</label>
        <input type="text" class="form-control" name="passwords" id="passwords" placeholder="Input field">
    </div>

    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
</form>
<?php
if(isset($_POST['submit'])){
    $user = new User_CRUD($pdo); // Assuming $pdo is your PDO instance from connection.php
    $data = [
        'username' => $_POST['username'],
        'email' => $_POST['email'],
        'password' => $_POST['passwords']
    ];
    $user->createUser($data['username'], $data['email'], $data['password']);
}

?>
    
</body>
</html>