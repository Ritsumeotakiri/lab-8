<?php
// Initialize PDO connection and other necessary configurations

// Handle form submissions
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["add_post"])) {
        // Add post logic
        // Use filter_var to validate inputs
        $title = filter_var($_POST["title"], FILTER_SANITIZE_STRING);
        $content = filter_var($_POST["content"], FILTER_SANITIZE_STRING);
        // Add post to database
    } elseif (isset($_POST["add_comment"])) {
        // Add comment logic
        // Use filter_var to validate inputs
        $postId = filter_var($_POST["postId"], FILTER_SANITIZE_NUMBER_INT);
        $comment = filter_var($_POST["comment"], FILTER_SANITIZE_STRING);
        // Add comment to database
    } elseif (isset($_POST["rate_post"])) {
        // Rate post logic
        // Use filter_var to validate inputs
        $postId = filter_var($_POST["postId"], FILTER_SANITIZE_NUMBER_INT);
        $rating = filter_var($_POST["rating"], FILTER_SANITIZE_NUMBER_INT);
        // Add rating to database
    }
}

// Display the page content
?>

<!DOCTYPE html>
<html>
<head>
    <title>Blog</title>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<body>
    <h1>Welcome to the Blog</h1>

    <?php if (!isset($_GET["post_id"])): ?>
        <!-- Display list of posts -->
        <h2>Posts</h2>
        <ul>
            <li><a href="?post_id=1">Post 1 - Title</a></li>
            <!-- Add more posts -->
        </ul>
    <?php else: ?>
        <!-- Display post details -->
        <?php
        $postId = filter_var($_GET["post_id"], FILTER_SANITIZE_NUMBER_INT);
        // Fetch post details from database
        ?>
        <h2>Post Title</h2>
        <p>Post content...</p>

        <h3>Comments</h3>
        <!-- Display comments -->
        <ul>
            <li>Comment 1</li>
            <!-- Add more comments -->
        </ul>

        <!-- Add comment form -->
        <h3>Add Comment</h3>
        <form method="post" action="">
            <input type="hidden" name="postId" value="<?php echo $postId; ?>">
            <textarea name="comment" placeholder="Your Comment" required></textarea><br>
            <input type="submit" name="add_comment" value="Add Comment">
        </form>

        <!-- Add rating form -->
        <h3>Rate Post</h3>
        <form method="post" action="">
            <input type="hidden" name="postId" value="<?php echo $postId; ?>">
            <select name="rating">
                <option value="1">1 Star</option>
                <option value="2">2 Stars</option>
                <option value="3">3 Stars</option>
                <option value="4">4 Stars</option>
                <option value="5">5 Stars</option>
            </select><br>
            <input type="submit" name="rate_post" value="Rate">
        </form>
    <?php endif; ?>

    <!-- Add post form -->
    <h2>Add Post</h2>
    <form method="post" action="">
        <input type="text" name="title" placeholder="Post Title" required><br>
        <textarea name="content" placeholder="Post Content" required></textarea><br>
        <input type="submit" name="add_post" value="Add Post">
    </form>
</body>
</html>
