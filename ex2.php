<?php
include("connection.php");

class User_CRUD {
    private $pdo;

    public function __construct(PDO $pdo) {
        $this->pdo = $pdo;
    }

    public function createUser($username, $email, $passwords) {
        $stmt = $this->pdo->prepare("INSERT INTO users (username, email, passwords) VALUES (:username, :email, :passwords)");
        $stmt->execute(['username' => $username, 'email' => $email, 'passwords' => $passwords]);
        return $this->pdo->lastInsertId();
    }

    public function readUser($id) {
        $stmt = $this->pdo->prepare("SELECT * FROM users WHERE id = :id");
        $stmt->execute(['id' => $id]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function updateUser($id, $username, $email, $password) {
        $stmt = $this->pdo->prepare("UPDATE users SET username = :username, email = :email, password = :password WHERE id = :id");
        $stmt->execute(['id' => $id, 'username' => $username, 'email' => $email, 'password' => $password]);
        return $stmt->rowCount();
    }

    public function deleteUser($id) {
        $stmt = $this->pdo->prepare("DELETE FROM users WHERE id = :id");
        $stmt->execute(['id' => $id]);
        return $stmt->rowCount();
    }
}

class Post_CRUD {
    private $pdo;

    public function __construct(PDO $pdo) {
        $this->pdo = $pdo;
    }

    public function create($title, $content, $authorId, $createdAt) {
        $stmt = $this->pdo->prepare("INSERT INTO posts (title, content, author_id, created_at) VALUES (:title, :content, :author_id, :created_at)");
        $stmt->execute(['title' => $title, 'content' => $content, 'author_id' => $authorId, 'created_at' => $createdAt]);
        return $this->pdo->lastInsertId();
    }

    public function read() {
        $stmt = $this->pdo->query("SELECT * FROM posts");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function update($id, $title, $content) {
        $stmt = $this->pdo->prepare("UPDATE posts SET title = :title, content = :content WHERE id = :id");
        $stmt->execute(['id' => $id, 'title' => $title, 'content' => $content]);
        return $stmt->rowCount() > 0;
    }

    public function delete($id) {
        $stmt = $this->pdo->prepare("DELETE FROM posts WHERE id = :id");
        $stmt->execute(['id' => $id]);
        return $stmt->rowCount() > 0;
    }
}

class Comment_CRUD {
    private $pdo;

    public function __construct(PDO $pdo) {
        $this->pdo = $pdo;
    }

    public function createComment($postId, $userId, $comment) {
        $stmt = $this->pdo->prepare("INSERT INTO comments (post_id, user_id, comment) VALUES (:post_id, :user_id, :comment)");
        $stmt->execute(['post_id' => $postId, 'user_id' => $userId, 'comment' => $comment]);
        return $this->pdo->lastInsertId();
    }

    public function readComment($id) {
        $stmt = $this->pdo->prepare("SELECT * FROM comments WHERE id = :id");
        $stmt->execute(['id' => $id]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function updateComment($id, $comment) {
        $stmt = $this->pdo->prepare("UPDATE comments SET comment = :comment WHERE id = :id");
        $stmt->execute(['id' => $id, 'comment' => $comment]);
        return $stmt->rowCount();
    }

    public function deleteComment($id) {
        $stmt = $this->pdo->prepare("DELETE FROM comments WHERE id = :id");
        $stmt->execute(['id' => $id]);
        return $stmt->rowCount();
    }
}

class Rate_CRUD {
    private $pdo;

    public function __construct(PDO $pdo) {
        $this->pdo = $pdo;
    }

    public function createRating($postId, $userId, $rating) {
        $stmt = $this->pdo->prepare("INSERT INTO ratings (post_id, user_id, rating) VALUES (:post_id, :user_id, :rating)");
        $stmt->execute(['post_id' => $postId, 'user_id' => $userId, 'rating' => $rating]);
        return $this->pdo->lastInsertId();
    }

    public function readRating($id) {
        $stmt = $this->pdo->prepare("SELECT * FROM ratings WHERE id = :id");
        $stmt->execute(['id' => $id]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function updateRating($id, $rating) {
        $stmt = $this->pdo->prepare("UPDATE ratings SET rating = :rating WHERE id = :id");
        $stmt->execute(['id' => $id, 'rating' => $rating]);
        return $stmt->rowCount();
    }

    public function deleteRating($id) {
        $stmt = $this->pdo->prepare("DELETE FROM ratings WHERE id = :id");
        $stmt->execute(['id' => $id]);
        return $stmt->rowCount();
    }
}
?>
